package com.example.webshop.domain.model

data class Product(
    val name: String,
    val type: Type,
    val price: Double,
    val image: String?,
    val description: String?,
    val available: Boolean
)