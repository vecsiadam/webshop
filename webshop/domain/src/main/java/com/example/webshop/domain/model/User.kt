package com.example.webshop.domain.model

import com.example.webshop.domain.model.Product
import com.example.webshop.domain.model.Role

data class User(
    val username: String,
    val password: String,
    val address: String,
    val email: String,
    val role: Role,
    val status: Boolean?,
    val productsInCart: List<Product>
)
