package com.example.webshop.domain.model

enum class Type {
    APPLIANCE,
    TV,
    COMPUTER,
    MOBILE,
    TABLET,
    AUDIO,
    CAR_ELECTRONICS,
    VIDEO_GAMES,
    MOVIES
}