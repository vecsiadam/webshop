package com.example.webshop.domain.model

enum class Role {
    ADMIN,
    USER
}