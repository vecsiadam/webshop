# Mobile programing - webshop

##### Leírás:
Egy egyetemi tárgy, a Mobil programozás keretein belül szeretnénk létrehozni, egy olyan Android alkalmazást Kotlin nyelven ami egy egyszerű webshop-ot valósít meg. Ez az alkalmazást Szerepkör (role) alapján dönti el, hogy milyen funkciókat érünk el vagy éppen nem érünk el. Lentebb látható egy elképzelés az alkalmazással kapcsolatban.

- Adatbázis táblák:
    - Users: felhasználók tárolására [username:String, password:String , address:String , email:String , role:Role, status:Boolean, productInCart:List<String>] (tudom csúnya de a pass az lehet telibe tárolva nem elhashelve, csak hogy ne nehezítsük a dolgunk)
    - Products: termékek tárolására [name:String, type:Type, price:Double, image:String, description:string, available:Boolean]

- Felületek:
    - Bejelentkezés:
        - Bejelentkezéshez egy felület (username, password) //nincs regisztráció kézzel felvisszünk egy admint meg egy usert és kész.
        - Itt megkapjuk a bejelentkezéskor a felhasználó role-ját ami alapján a felületet variáljuk.
        - Ha admin több funkció elérhető. (Termék módosítás, termék feltöltés, rendelés visszaigazolás)
    - Kereső oldal:
        - Egyszerű keresést valósít meg lista szerűen jelenítjük meg pl [name, type, price,image]
        - Keresés: névben
        - Rendezés: tipus szerint, esetleg egy abc meg egy ár szerinti növekvő csökkenő rendezés???
    - Termék végoldal:
        - Itt látszódna a termékről minden infó, itt lehetne egy “Kosárba” gomb
        - Ha a roleunk admin itt lehetne egy szerkesztés gomb, nyilván ha sima kliens akkor nem látszik ez a gomb
    - Kosár:
        - Itt láthatjuk az összes kosárba rakott terméket lista szerűen, mint a Kereső oldalnál kb, annyival kiegészítve hogy a végén lesz egy összesen ár. A kosár tartalma db-ben lesz, egy many-to-many kapcsolat lesz a felhasználó és termék között. Amikor berakunk a korsárba egy terméket akkor a felhasználóhoz rendeljük azt a terméket.
        - Megrendelés gomb -> ennek hatására kimegy az adminnak egy email, valamint ha az admin available akkor push notification is megy neki
    - Termék feltöltés (admin)
        - Újtermék feltöltése.

- Végpontok:
    - Összes termék.
    - Termékek típus alapján.
    - Termék hozzáadás.
    - Termék módosítás.
    - Termék törlés.
    - User felhnév és pass alapján.
    - Userek role és status alapján.
    - Termék hozzáadása felhasználóhoz.
    - Termék törlés felhasználótól.
    - Felhasználóhoz adott termékek listázása.